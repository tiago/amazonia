## De-amazon yourself

**Woodworking**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://ardec.ca/|Ardec is a Canadian online store, we sell products for wood finishing and restoration. |Free shipping for orders over $90 · Quebec, Ontario|Saint-Sauveur (QC)|
|https://www.langevinforest.com/|Langevin Forest provides their clientele with a vast selection of exotic and indigenous wood, as well as tools, accessories, and finishing products to help you be successful with all your projects, big or small.||Montreal|

**Computer stuff**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://www.mikescomputershop.com|             |          |           |
|https://www.upsbatterycenter.ca  |             |          |           |
|https://www.dtcinformatique.ca   |             |          |           |

**Music/Audio/Video**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://www.crutchfield.ca   |    |    |      |
|https://www.long-mcquade.com |    |    |      |


**House**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://www.paulsfinest.com|Great knifes selection|          |Montreal|
|https://www.partselect.ca|Good to find old appliances parts|          |           |
|https://www.forumappliances.com|Good to find old appliances parts|          |BC         |

**Electronics**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://canadabolts.ca|             |          |           |
|https://abra-electronics.com|             |          |           |
|https://www.digikey.ca|             |          |           |
|https://www.mouser.ca|             |          |           |
|http://dipmicro.com/store|             |          |           |
|https://www.canakit.com|             |          |           |

**3D printing**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://filaments.ca|             |          |           |
|https://spool3d.ca|             |          |           |
|https://nefilatek.com|             |          |           |
|https://filaments3dquebec.ca|             |          |           |
|https://www.th3dstudio.com|             |          |           |


**Power tools**

| website | description | shipping | warehouse |
|---------|-------------|----------|-----------|
|https://www.busybeetools.com|             |          |           |
|https://www.leevalley.com/en-ca|             |          |           |
|https://www.patrickmorin.com|             |          |           |
|https://www.canac.ca|             |          |           |
